package com.market.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import javax.sql.DataSource;

@Configuration  //Anotasi untukmenyatakanbahwa class ini adalah class untuk Knfigurasi
@EnableWebSecurity  //Mengaktifkan fasilitas WebSecurity di class ini
public class SecurityConfiguration extends WebSecurityConfigurerAdapter { // extends berarti Class SecurityConfiguration berisi juga fungsi" dari WebSecurityConfigurerAdapter

    @Autowired  //Anotasi untuk menyatakan bean variable yang ada dibawahnya
    private BCryptPasswordEncoder bCryptPasswordEncoder; //Deklarasi encode password

    @Autowired
    private DataSource dataSource; //deklarasi datasource

    @Value("${spring.queries.users-query}") //Mengambil value dari file application.properties yang ada di folder resource
    private String usersQuery;

    @Value("${spring.queries.roles-query}")
    private String rolesQuery;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) //Konfigurasi akses login
            throws Exception {
        auth.
                jdbcAuthentication()
                .usersByUsernameQuery(usersQuery)
                .authoritiesByUsernameQuery(rolesQuery)
                .dataSource(dataSource)
                .passwordEncoder(bCryptPasswordEncoder);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception { //Konfigurasi akses URL

        http.
                authorizeRequests()
                .antMatchers("/").permitAll()
                .antMatchers("/login").permitAll()
                .antMatchers("/api/user/**").permitAll()    //URL yang dapat diakses tanpa login
                .antMatchers("/admin/**", "/setting/admin").hasRole("ADMIN")    //URL yang perlu akses ADMIN
                //.antMatchers("/kasir").hasRole("KASIR")
                .anyRequest().authenticated()
                .and().csrf().disable().formLogin()
                .loginPage("/login").failureUrl("/login?error=true")    //URL jika login gagal
                .defaultSuccessUrl("/kasir")    //URL setelah berhasil login
                .usernameParameter("username")
                .passwordParameter("password")
                .and().logout()
                .logoutRequestMatcher(new AntPathRequestMatcher("/logout")) //URL untuk logout
                .logoutSuccessUrl("/login").and().exceptionHandling()   //URL setelah logout berhasil
                .accessDeniedPage("/denied");   //URL jika memasuki URL yang tidak sesuai akses
    }

    @Override
    public void configure(WebSecurity web) throws Exception {   //Konfigurasi web 2
        web
                .ignoring()
                .antMatchers("/resources/**", "/static/**", "/css/**", "/js/**", "/img/**","/scss/**","/fonts/**");
    }

}

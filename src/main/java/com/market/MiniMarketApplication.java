package com.market;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@SpringBootApplication	//Aplikasi Spring Boot
public class MiniMarketApplication {

	public static void main(String[] args) {	//Fungsi default dari Java yang akan dijalankan pertama
		SpringApplication.run(MiniMarketApplication.class, args);
	}
}
